from django.contrib import admin

from .models import PostSubcategory, PostCategory, Post, Comment


class PostSubcategoryAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Subcategory Name', {'fields': ['name', 'category']})
    ]
    list_display = ('name', 'created')
    search_fields = ['name']
    ordering = ['-name']

admin.site.register(PostSubcategory, PostSubcategoryAdmin)


class PostCategoryAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Category Name', {'fields': ['name']})
    ]
    list_display = ('name', 'created')
    search_fields = ['name']
    ordering = ['-name']

admin.site.register(PostCategory, PostCategoryAdmin)


class PostAdmin(admin.ModelAdmin):
    fieldsets = [
        #('Author', {'fields': ['author']}),
        ('Article Title and summary', {'fields': ['title', 'subcategory', 'slug', 'image', 'summary', 'body', 'featured', 'tags']}),
        ('Publish Date and Time', {'fields': ['publish', 'status']}),
    ]
    list_display = ('author', 'title', 'category', 'subcategory', 'slug', 'publish', 'status')
    list_filter = ('category', 'subcategory', 'status', 'created', 'publish', 'author')
    search_fields = ('title', 'body')
    prepopulated_fields = {'slug': ('title',)}
    raw_id_fields = ('author',)
    date_hierarchy = 'publish'
    ordering = ['-status', '-publish']

    def save_model(self, request, obj, form, change):
        if getattr(obj, 'author', None) is None:
            obj.author = request.user
        obj.save()

admin.site.register(Post, PostAdmin)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'post', 'created', 'active')
    list_filter = ('active', 'created', 'updated')
    search_fields = ('name', 'email', 'body')

admin.site.register(Comment, CommentAdmin)

