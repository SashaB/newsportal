from django.contrib.syndication.views import Feed
from django.template.defaultfilters import truncatewords

from blog.models import Post


class LatestPostFeed(Feed):
    title = 'News Portal'
    link = '/blog/'
    description = 'Latest news from portal'

    def items(self):
        return Post.published.all()[:25]

    def item_title(self, item):
        return item.title
    
    def item_description(self, item):
        return truncatewords(item.body, 30)
