import markdown

from django import template
from django.db.models import Count
from django.utils.safestring import mark_safe

register = template.Library()

from ..models import Post, PostCategory


@register.simple_tag
def total_posts():
    return Post.published.count()


@register.inclusion_tag('blog/post/latest_posts.html')
def show_latest_posts(count=5):
    latest_posts = Post.published.order_by('-publish')[:count]
    return {'latest_posts': latest_posts}


@register.inclusion_tag('blog/navbar_categories.html')
def show_categories():
    categories = PostCategory.objects.all().order_by('created')

    return {'categories': categories}


@register.assignment_tag
def get_most_commented_posts(count=5):
    total_comments = Post.published.annotate(total_comments=Count('comments')).order_by('-total_comments')[:count]
    return total_comments


@register.filter(name='markdown')
def markdown_format(text):
    return mark_safe(markdown.markdown(text))
