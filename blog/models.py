from django.db import models
from django.conf import settings
from django.utils import timezone
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from taggit.managers import TaggableManager
from ckeditor_uploader.fields import RichTextUploadingField


class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager, self).get_queryset().filter(status='published')


class PostCategory(models.Model):

    name = models.CharField(max_length=30, null=False, blank=False, unique=True)
    created = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-name',)

    def __str__(self):
        return self.name

    # def save(self, *args, **kwargs):
    #     self.name = self.name.capitalize()
    #     super(PostCategory, self).save(*args, **kwargs)


class PostSubcategory(models.Model):

    name = models.CharField(max_length=40, unique=True, blank=False, null=False)
    category = models.ForeignKey(PostCategory, related_name='category', null=False, blank=False)
    created = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-name',)

    def __str__(self):
        return self.name

    # def save(self, *args, **kwargs):
    #     self.name = self.name.capitalize()
    #     super(PostSubcategory, self).save(*args, **kwargs)


class Post(models.Model):
    STATUS_CHOICE = (
        ('draft', 'Draft'),
        ('published', 'Published'),
    )

    author = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='blog_posts', null=False, blank=True)
    category = models.ForeignKey(PostCategory, related_name='post_category', null=False, blank=False)
    subcategory = models.ForeignKey(PostSubcategory, related_name='post_subcategory', null=False, blank=False)
    title = models.CharField(max_length=250)
    slug = models.SlugField(max_length=250, unique_for_date='publish')
    image = models.ImageField(upload_to='uploads/post/%Y/%m/%d', blank=False, null=False)
    summary = models.TextField(default='summary')
    body = RichTextUploadingField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICE, default='draft')
    featured = models.BooleanField(default=0)

    tags = TaggableManager()

    objects = models.Manager()
    published = PublishedManager()

    class Meta:
        ordering = ('-publish',)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('blog:post_detail', args=[
            self.publish.year,
            self.publish.strftime('%m'),
            self.publish.strftime('%d'),
            self.slug
        ])

    def save(self, *args, **kwargs):
        self.category = self.subcategory.category
        super(Post, self).save(*args, **kwargs)


class Comment(models.Model):

    post = models.ForeignKey(Post, related_name='comments')
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return 'Comment by {0} on {1}'.format(self.name, self.post)
