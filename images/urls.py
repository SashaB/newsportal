from django.conf.urls import url
from . import views

urlpatterns = [
    url('^create/$', views.image_create, name='create'),
]