(function() {
    var jquery_version = '2.1.4';
    var site_url = 'http://127.0.0.1:8000/';
    var static_url = site_url + 'static/';
    var min_width = 100;
    var min_height = 100;

    function bookmarklet() {
        var css = window.jQuery('<link>');

        css.attr({
            rel: 'stylesheet',
            type: 'text/css',
            href: static_url + 'css/bookmarklet.css?r=' + Math.floor(Math.random() * 999999999999999)
        });
        jQuery('head').append(css)

        // load HTML
        box_html = '<div id="bookmarklet"><a href="#" id="close">&times;</a><h1>Select an image to bookmark:</h1><div class="images"></div></div>';
        $('body').append(box_html);
        // close event
        $('#bookmarklet #close').click(function() {
            $('#bookmarklet').remove();
        });

        // find images and display them
        jQuery.each(jQuery('img[src$="jpg"]'), function(index, image) {
            if (jQuery(image).width() >= min_width && jQuery(image).height() >= min_height) {
                image_url = jQuery(image).attr('src');
                jQuery('#bookmarklet .images').append('<a href="#"><img src="'+
                image_url +'" /></a>');
            }
        });

        // when an image is selected open URL with it
        $('#bookmarklet .images a').click(function(e) {
            selected_image = $(this).children('img').attr('src');
            // hide bookmarklet
            $('#bookmarklet').hide();
            // open new window to submit the image
            window.open(site_url +'images/create/?url=' + encodeURIComponent(selected_image) + '&title=' + encodeURIComponent($('title').text()), '_blank');
        });

    };

    // check if jQuery is loaded
    if (typeof window.jQuery != 'undefined') {
        bookmarklet();
    } else {
        // check if jQuery is loaded
        var conflict = typeof window.$ != 'undefined';
        // create the script
        var script = document.createElement('script');
        script.setAttribute('src', 'https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js');
        document.getElementsByTagName('head')[0].appendChild(script);

        var attempts = 15;
        (function() {
            if (typeof window.jQuery != 'undefined') {
                if (--attempts > 0) {
                    window.setTimeout(arguments.callee, 250);
                } else {
                    alert('An error ocurred while loading jQuery');
                }
            } else {
                bookmarklet();
            }
        }) ();
    }
}) ();