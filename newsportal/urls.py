from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.sitemaps.views import sitemap

from blog.sitemaps import PostSitemap

sitemaps = {
    'posts': PostSitemap,
}

urlpatterns = [
    # admin login and urls
    url(r'^admin/', include(admin.site.urls)),
    # user and admin login urls
    url(r'^account/', include('account.urls')),
    # blog urls
    url(r'^blog/', include('blog.urls', namespace='blog')),
    # sitemap url
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    # ckeditor urls
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    # images share - demo
    url(r'^images/', include('images.urls', namespace='images')),
    # social authentication urls
    url(r'^social-auth/', include('social.apps.django_app.urls', namespace='social')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
