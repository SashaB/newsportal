from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from .models import Profile


class AuthForm(AuthenticationForm):
    username = forms.CharField(label='Username or Email', strip=True, max_length=254, widget=forms.TextInput(attrs={
        'autofocus': '',
        'class': 'form-control'
    }))

    password = forms.CharField(label='Password', strip=False, widget=forms.PasswordInput(attrs={
        'class': 'form-control',
        #'placeholder': 'Password'
    }))


class PasswdChangeForm(PasswordChangeForm):
    old_password = forms.CharField(label='Old password', strip=False, widget=forms.PasswordInput(attrs={
        'autofocus': '',
        'class': 'form-control'
    }))

    password = forms.CharField(label='New password', strip=False, widget=forms.PasswordInput(attrs={
        'autofocus': '',
        'class': 'form-control'
    }))

    password2 = forms.CharField(label='Confirm new password', strip=False, widget=forms.PasswordInput(attrs={
        'autofocus': '',
        'class': 'form-control'
    }))


class UserRegistrationForm(forms.ModelForm):
    first_name = forms.CharField(label='First name', strip=True, max_length=80, required=True)
    email = forms.CharField(label='Email', widget=forms.EmailInput, required=True)
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'test'}))
    password2 = forms.CharField(label='Repeat password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        if cd['password'] != cd['password2']:
            raise forms.ValidationError('Passwords don\'t match.')
        return cd['password2']


class UserEditForm(forms.ModelForm):
    first_name = forms.CharField(label="First Name", widget=forms.TextInput(attrs={
        'class': 'float-right'
    }), required=True)
    last_name = forms.CharField(label="Last Name", required=False, widget=forms.TextInput(attrs={
        'class': 'float-right'
    }))
    email = forms.CharField(label="Email", widget=forms.TextInput(attrs={
        'class': 'float-right'
    }), required=True)

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileEditForm(forms.ModelForm):
    date_of_birth = forms.DateField(label='Date of birth', required=False, widget=forms.DateInput(attrs={
        'class': 'float-right'
    }))

    photo = forms.ImageField(label='Change a photo', required=False, widget=forms.FileInput(attrs={
        'class': 'float-right'
    }))

    class Meta:
        model = Profile
        fields = ('date_of_birth', 'photo')
