from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.views import password_reset as django_password_reset
from django.core.exceptions import ObjectDoesNotExist

from .models import Profile
from .forms import UserRegistrationForm, UserEditForm, ProfileEditForm


@login_required
def welcome(request):
    try:
        Profile.objects.get(user=request.user)
    except ObjectDoesNotExist:
        Profile.objects.create(user=request.user)

    return HttpResponseRedirect(reverse('blog:post_list'))


@login_required
def dashboard(request):
    return render(request, 'account/dashboard.html', {'section': 'dashboard'})


@login_required
def edit(request):
    try:
        user_profile = Profile.objects.get(user=request.user)
    except ObjectDoesNotExist:
        user_profile = Profile.objects.create(user=request.user)

    if request.method == 'POST':
        user_form = UserEditForm(instance=request.user, data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile, data=request.POST, files=request.FILES)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Profile updated successfully')
            return HttpResponseRedirect(reverse('edit'))
        else:
            messages.error(request, 'Error updating your profile')
    else:
        user_form = UserEditForm(instance=request.user)
        profile_form = ProfileEditForm(instance=request.user.profile)

    #user_profile = Profile.objects.get(user=request.user)

    return HttpResponse(render(request, 'account/edit.html', {
        'user_form': user_form,
        'profile_form': profile_form,
        'user_profile': user_profile
    }))


def password_reset(request, *args, **kwargs):
    email_template_name = 'registration/password_reset_email.html'
    return django_password_reset(request, html_email_template_name=email_template_name, *args, **kwargs)


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # create user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])
            # save the user object
            new_user.save()

            # create the user profile
            profile = Profile.objects.create(user=new_user)

            return render(request, 'account/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()

    return render(request, 'account/register.html', {'user_form': user_form})
